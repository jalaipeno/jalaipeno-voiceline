import { Router } from "express"
import monk from "monk"
const Joi = require("@hapi/joi");

const db = monk(process.env.MONGO_URI as string)
const voicelines = db.get("voicelines")

const router = Router();

const voiceLineSchema = Joi.object({
  transcript: Joi.string().required(),
  summary: Joi.object().required(),
  tasks: Joi.array().items(Joi.string()).required()
});

//READ ALL
router.get(
  "/",
  async (req, res, next) => {
    try {
      const items = await voicelines.find({});
      res.json(items);
    } catch (error) {
      next(error);
    }
  }
);

//READ One
router.get(
  "/:id",
  async (req, res, next) => {
    try {
      const { id } = req.params
      const item = await voicelines.findOne({
        _id: id,
      })
      if (!item) next()
      res.json(item)
    } catch (error) {
      next(error);
    }
  }
)

//Create One
router.post(
  "/",
  async (req, res, next) => {
    try {
      console.log(req.body);
      const value = await voiceLineSchema.validateAsync(req.body)
      const inserted = await voicelines.insert(value)
      res.json(inserted);
    } catch (error) {
      next(error);
    }
  }
)

//Update One
router.put(
  "/:id",
  async (req, res, next) => {
    try {
      const { id } = req.params
      const value = await voiceLineSchema.validateAsync(req.body)
      const item = await voicelines.findOne({
        _id: id,
      })
      if (!item) next()
      const updated = await voicelines.update({
        _id: id,
      }, {
        $set: value
      })
      res.json(updated);
    } catch (error) {
      next(error);
    }
  }
)

//Delete One
router.delete(
  "/",
  async (req, res, next) => {
    try {
      const { id } = req.params;
      await voicelines.remove({ _id: id})
      res.json({
        messaage: "Success",
      })
    } catch (error) {
      next(error)
    }
  }
)

export { router as voiceLineRouter };

import e, { Router } from "express";
import {
  VoiceLine,
  VoiceLineTask,
  VoiceLineSummary,
  VoiceLineFeedback,
  VoiceLineContent,
} from "../models/voiceLine";
import { EnrichManager } from "../tools/enrichManager";

const router = Router();

router.post("/", async (req, res) => {
  // 1. summarize provided transcript with given options
  const summary: VoiceLineSummary = await EnrichManager.summarize(
    req.body.transcript
  );

  // 2. extract tasks
  const tasks: VoiceLineTask[] = await EnrichManager.classify(
    summary.bulletSummary,
    req.body.options
  );

  // Initialize response
  let response: VoiceLine = {
    transcript: req.body.transcript,
    content: { summary: summary, tasks: tasks },
  };

  // 3. carry out tasks
  let email: string[] = [];
  let genericTasks: string = "";
  let appointment: string = "";
  let feedback = {};

  await Promise.all(
    tasks.map(async (task: VoiceLineTask) => {
      switch (task) {
        case VoiceLineTask.MAIL:
          email = await EnrichManager.createMail(
            summary.textSummary,
            req.body.options
          );
          response = {
            ...response,
            content: { ...response.content, mail: email },
          };
          break;
        case VoiceLineTask.GENERIC:
          genericTasks = await EnrichManager.createGenericTasks(
            summary.textSummary,
            req.body.options
          );

          response = {
            ...response,
            content: { ...response.content, genericTask: genericTasks },
          };

          break;
        case VoiceLineTask.FEEDBACK:
          feedback = await EnrichManager.createFeedback(
            summary.textSummary,
            req.body.options
          );

          response = {
            ...response,
            content: {
              ...response.content,
              feedback: feedback as VoiceLineFeedback,
            },
          };
          break;
        case VoiceLineTask.APPOINTMENT:
          appointment = await EnrichManager.createAppointment(
            summary.textSummary,
            req.body.options
          );

          response = {
            ...response,
            content: {
              ...response.content,
              appointment: appointment as string,
            },
          };
          break;
      }
    })
  );

  //Creating a database entry
  try {
    const vc = await VoiceLine.create(response);
    response.vl_id = vc._id
  } catch (error) {
    console.log("Database error")
  }

  res.setHeader("Content-Type", "application/json");
  res.json(response);
});

export { router as enrichRouter };

import {
  VoiceLine,
  VoiceLineTask,
  VoiceLineSummary,
  VoiceLineFeedback,
} from "../models/voiceLine";
import fetch from "cross-fetch";

import bulletSummaryParams from "../templates/summary/summary_in_bullets.json";
import textSummaryParams from "../templates/summary/summary_from_text.json";
import classifySummaryParams from "../templates/classification/classification_from_bullets.json";
import createMailParams from "../templates/mail/mail_textsummary_prompt.json";
import createGenericTasksParams from "../templates/generic/task_list_prompt.json";
import createFeedbackBulletParams from "../templates/feedback/feedback_bulletsummary_prompt.json";
import createFeedbackTextParams from "../templates/feedback/feedback_textsummary_prompt.json";
import createAppointmentParams from "../templates/appointment/meeting_extraction_prompt.json";

type APIRequestParams = {
  prompt: string;
  max_tokens: number;
  temperature: number;
  top_p: number;
  logprobs: any;
  stop: string;
  stream: boolean;
  frequency_penalty: number;
};

const completionUrl = (engine: string) => {
  return `https://api.openai.com/v1/engines/${engine}/completions`;
};

const replacePrompt = (prompt: string, s: string) => {
  return prompt.replace("{0}", s);
};

async function gptAPICall(query: string, params: any) {
  const apiParams: APIRequestParams = bundleParams(params, query);

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${process.env.OPENAI_SECRET_KEY as string}`,
    },
    body: JSON.stringify(apiParams),
  };

  return await fetch(completionUrl(params.engine), requestOptions)
    .then((response) => {
      if (response.status >= 400) {
        throw new Error(
          response.status +
            " - Bad response from GPT3 Server: " +
            response.statusText
        );
      }
      return response.json();
    })
    .then((data: any) => {
      // Create VoiceLine object out of gotten enrichment from GPT3
      console.log(data.choices[0].text.trim());
      console.log();
      return data.choices[0].text;
    })
    .catch((err) => {
      console.error(err);
      return err;
    });
}

const bundleParams = (params: any, transcript: string): APIRequestParams => {
  return {
    prompt: replacePrompt(params.prompt, transcript),
    max_tokens: params.max_tokens,
    temperature: params.temperature,
    top_p: params.top_p,
    logprobs: params.logprobs,
    stop: params["stop s"],
    frequency_penalty: params.frequency_penalty,
    stream: false,
  };
};

export class EnrichManager {
  static async summarize(transcript: string): Promise<VoiceLineSummary> {
    const bulletSummary = await gptAPICall(transcript, bulletSummaryParams);

    const textSummary = await gptAPICall(transcript, textSummaryParams);

    return {
      bulletSummary: bulletSummary.trim(),
      textSummary: textSummary.trim(),
    };
  }

  static async classify(
    bulletSummary: string,
    options: any
  ): Promise<VoiceLineTask[]> {
    const tasks = await gptAPICall(bulletSummary, classifySummaryParams);

    let taskList: VoiceLineTask[] = [];
    tasks
      .trim()
      .split(",")
      .map((element: string) => {
        switch (element.trim().toLowerCase()) {
          case "email":
            taskList = [...taskList, VoiceLineTask.MAIL];
            break;
          case "generic":
            taskList = [...taskList, VoiceLineTask.GENERIC];
            break;
          case "appointment":
            taskList = [...taskList, VoiceLineTask.APPOINTMENT];
            break;
          case "feedback":
            taskList = [...taskList, VoiceLineTask.FEEDBACK];
            break;
        }
      });

    return taskList;
  }

  static async createMail(
    textSummary: string,
    options: any
  ): Promise<string[]> {
    const mail = await gptAPICall(textSummary, createMailParams);
    return mail.trim().split("\n");
  }

  static async createGenericTasks(
    textSummary: string,
    options: any
  ): Promise<string> {
    const genericTask = await gptAPICall(textSummary, createGenericTasksParams);
    return genericTask.trim();
  }

  static async createAppointment(
    textSummary: string,
    options: any
  ): Promise<string> {
    const appointment = await gptAPICall(textSummary, createAppointmentParams);

    return appointment.trim();
  }

  static async createFeedback(
    textSummary: string,
    options: any
  ): Promise<VoiceLineFeedback> {
    const bulletFeedback = await gptAPICall(
      textSummary,
      createFeedbackBulletParams
    );

    const textFeedback = await gptAPICall(
      textSummary,
      createFeedbackTextParams
    );

    return {
      bulletFeedback: bulletFeedback.trim(),
      textFeedback: textFeedback.trim(),
    };
  }
}

import monk from "monk"
const Joi = require("@hapi/joi");
const collection_name = "voicelines"

const schema = Joi.object();

const db = monk(process.env.MONGO_URI as string)
const dbcollection = db.get(collection_name)

export enum VoiceLineTask {
  MAIL = 0,
  GENERIC = 1,
  FEEDBACK = 2,
  APPOINTMENT = 3,
}

export type VoiceLineSummary = {
  bulletSummary: string;
  textSummary: string;
};

export type VoiceLineFeedback = {
  bulletFeedback: string;
  textFeedback: string;
};

export type VoiceLineContent = {
  /* Dictionary of summaries of the given transcript. 
    All following tasks are carried out on basis of this provided summary */
  summary: VoiceLineSummary;

  /* List of detected tasks */
  tasks: VoiceLineTask[];

  appointment?: string;

  genericTask?: string;

  /* List of strings which define a mail: subject, content, recipient */
  mail?: string[];

  /* Detected Feedback on the provided voiceline in bullets and text */
  feedback?: VoiceLineFeedback;
};

export class VoiceLine {
  /* unique voiceline id*/
  vl_id?: string;

  /* transcribed voice memo as string in original language */
  transcript: string;

  /* general content of the gpt3 api calls */
  content: VoiceLineContent;

  constructor(vl_id: string, transcript: string, content: VoiceLineContent) {
    this.vl_id = vl_id;
    this.transcript = transcript;
    this.content = content;
  }

  // CRUD Operations
  //Read all
static async getAll() {
    try {
        const items = await dbcollection.find({});
        return items
    } catch (error) {
        console.log(error)
    }
}

//Read One
static async get(id:number) {
    try {
        const item = await dbcollection.findOne({
            _id: id,
        })
        if(!item) throw new Error("object with id not found")
        return item
    } catch (error) {
        console.log(error)
    }
}

//Create One
static async create(request:any) {
    try {
        const value = await schema.validateAsync(request)
        const inserted = await dbcollection.insert(value)
        return inserted
    } catch (error) {
        console.log(error)
    }
}

//Update One
static async update(request:any, id:number) {
    try {
        const value = await schema.validateAsync(request)
        const item = await dbcollection.findOne({
          _id: id,
        })
        if (!item) throw Error()
        const updated = await dbcollection.update({
          _id: id,
        }, {
          $set: value
        })
        return updated
    } catch (error) {
        console.log(error)
    }
}

//Delete One
static async delete(id:number) {
    try {
        await dbcollection.remove({ _id: id})
        return {message: "Success"}
    } catch (error) {
        console.log(error)
    }
}

}

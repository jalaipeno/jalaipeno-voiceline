import monk from "monk";
const Joi = require("@hapi/joi");

const collection_name = "generic_tasks";

const schema = Joi.object();

const db = monk(process.env.MONGO_URI as string);
const dbcollection = db.get(collection_name);

export class GenericTask {
  //-----------After this point is generic------------------

  //Read all
  static async getAll() {
    try {
      const items = await dbcollection.find({});
      return items;
    } catch (error) {
      console.log(error);
    }
  }

  //Read One
  static async get(id: number) {
    try {
      const item = await dbcollection.findOne({
        _id: id,
      });
      if (!item) throw new Error("object with id not found");
      return item;
    } catch (error) {
      console.log(error);
    }
  }

  //Create One
  static async create(request: any) {
    try {
      const value = await schema.validateAsync(request);
      const inserted = await dbcollection.insert(value);
      return inserted;
    } catch (error) {
      console.log(error);
    }
  }

  //Update One
  static async update(request: any, id: number) {
    try {
      const value = await schema.validateAsync(request);
      const item = await dbcollection.findOne({
        _id: id,
      });
      if (!item) throw Error();
      const updated = await dbcollection.update(
        {
          _id: id,
        },
        {
          $set: value,
        }
      );
      return updated;
    } catch (error) {
      console.log(error);
    }
  }

  //Delete One
  static async delete(id: number) {
    try {
      await dbcollection.remove({ _id: id });
      return { message: "Success" };
    } catch (error) {
      console.log(error);
    }
  }
}

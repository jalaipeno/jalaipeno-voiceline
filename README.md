# JalAIpeño

hot, hotter, jalAIpeno :hot_pepper:

![](screenshots/screenshot4.png)

## Deploy Instructions

### Environment Variables
to correctly setup the environment you need to create a `.env` file in the `backend` folder that looks like this:
```
MONGO_URI=[this is the link to the mongo db instance]
OPENAI_SECRET_KEY=[this is the GPT3 secret key]
```
### Requirements

- docker-compose

### Command
- `git clone https://gitlab.com/jalaipeno/jalaipeno-voiceline.git`
- `cd jalaipeno-voiceline`
- `docker-compose build && docker-compose up`

### Exposed ports
- Port `4242` for the backend
- Port `3001` for the frontend

import React, { useState } from 'react';
import './App.css';
import {
    FormGroup,
    Label,
    Input,
    Card,
    CardTitle,
    Form,
    CardBody,
    Button,
    Spinner,
} from 'reactstrap';

import ResultOverview, {
    ResultOverviewOptions,
} from './components/ResultOverview/ResultOverview';

function App() {
    const [options, setOptions] = useState<ResultOverviewOptions>({
        language: 'english',
        style: 'textSummary',
    });

    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');
    const [text, setText] = useState<string>('');
    const [gptResult, setGPTResult] = useState<any>({});

    const submitSettings = (e: any) => {
        e.preventDefault();
        setLoading(true);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ transcript: text, options: options }),
        };

        fetch('http://localhost:4242/enrich', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                setGPTResult(data);
                setLoading(false);
            })
            .catch((err) => {
                setError(err);
                console.log(err);
            });
    };

    const changeLanguage = (e: any) => {
        setOptions({
            ...options,
            language: e.target.value,
        });
    };

    const changeStyle = (e: any) => {
        setOptions({
            ...options,
            style: e.target.value,
        });
    };

    return (
        <div className="settings">
            <Card>
                <CardBody>
                    <CardTitle tag="h4">Settings</CardTitle>
                    <Form>
                        <FormGroup>
                            <Label for="transcribedText">
                                Transcribed VoiceMemo
                            </Label>
                            <Input
                                type="textarea"
                                name="text"
                                id="transcribedText"
                                style={{ height: '10rem' }}
                                onChange={(e) => setText(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="languageSelect">Language</Label>
                            <Input
                                type="select"
                                name="select"
                                id="languageSelect"
                                onChange={changeLanguage}
                            >
                                <option value="english">English</option>
                                <option value="french">French</option>
                                <option value="spanish">Spanish</option>
                                <option value="german">German</option>
                                <option value="hindi">Hindi</option>
                                <option value="chinese">Chinese</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="styleSelect">
                                VoiceLine Summary Style
                            </Label>
                            <Input
                                type="select"
                                name="select"
                                id="styleSelect"
                                onChange={changeStyle}
                            >
                                <option value="textSummary">
                                    Short Summary
                                </option>
                                <option value="bulletSummary">
                                    Bullet Summary
                                </option>
                            </Input>
                        </FormGroup>

                        <Button
                            disabled={loading || !text}
                            onClick={submitSettings}
                        >
                            Save
                        </Button>
                        {loading && (
                            <Spinner
                                style={{
                                    marginLeft: '1rem',
                                }}
                                size="sm"
                                type="grow"
                                color="warning"
                            />
                        )}

                        <br />
                        <br />

                        {Object.keys(gptResult).length > 0 && (
                            <ResultOverview
                                GPTResponse={gptResult}
                                options={options}
                            />
                        )}
                    </Form>
                </CardBody>
            </Card>
        </div>
    );
}

export default App;

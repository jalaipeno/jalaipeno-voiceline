import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import './VoiceLineNavBar.css';

import VoiceLineLogo from './../../img/logo.svg';

const VoiceLineNavBar = (props: any) => {
    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">
                    <img src={VoiceLineLogo} alt="voiceline_logo" />
                </NavbarBrand>

                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <NavLink href="/">Documents</NavLink>
                    </NavItem>
                </Nav>
            </Navbar>
        </div>
    );
};

export default VoiceLineNavBar;

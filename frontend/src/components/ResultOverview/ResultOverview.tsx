import {
    VoiceLine,
    VoiceLineTask,
} from './../../../../backend/src/models/voiceLine';
import CalendarIMG from './../../img/calendar.png';
import FeedbackIMG from './../../img/feedback.png';
import GmailIMG from './../../img/gmail.png';
import TrelloIMG from './../../img/trello.png';
import { Badge, Card, CardBody, CardHeader, CardText } from 'reactstrap';
import './ResultOverview.css';

export interface ResultOverviewOptions {
    language: string;
    style: string;
}

export interface ResultOverviewProps {
    GPTResponse: VoiceLine;
    options: ResultOverviewOptions;
}

const ResultOverview = (props: ResultOverviewProps) => {
    let colorDict = {
        MAIL: 'primary',
        TASK: 'secondary',
        FEEDBACK: 'light',
        APPOINTMENT: 'success',
    };
    return (
        <div>
            {/* _tasks, appointment, genericTask, mail, feedback */}
            {console.log(Object.keys(props.GPTResponse.content))}

            {'tasks' in props.GPTResponse.content && (
                <div>
                    {props.GPTResponse.content.tasks.map(
                        (task: VoiceLineTask, idx: number) => {
                            return (
                                <Badge
                                    key={idx}
                                    color={
                                        Object.values(colorDict)[task.valueOf()]
                                    }
                                    style={{ marginRight: '0.5rem' }}
                                >
                                    #{Object.keys(colorDict)[task.valueOf()]}
                                </Badge>
                            );
                        },
                    )}
                </div>
            )}

            {'summary' in props.GPTResponse.content && (
                <Card style={{ marginTop: '1rem' }}>
                    <CardHeader>VoiceLine Summary</CardHeader>
                    <CardBody>
                        {props.options.style === 'bulletSummary' && (
                            <CardText>
                                {Object.values(
                                    props.GPTResponse.content.summary,
                                )[0]
                                    .split('\n')
                                    .map((bullet: string, idx: number) => {
                                        return (
                                            <li key={idx}>
                                                {bullet.replace('- ', '')}
                                            </li>
                                        );
                                    })}
                            </CardText>
                        )}
                        {props.options.style === 'textSummary' && (
                            <CardText>
                                {
                                    Object.values(
                                        props.GPTResponse.content.summary,
                                    )[1]
                                }
                            </CardText>
                        )}
                    </CardBody>
                </Card>
            )}

            {'appointment' in props.GPTResponse.content && (
                <div>
                    <img
                        className="integrationIMG"
                        src={CalendarIMG}
                        alt="calendar_img"
                    />
                    <p>{props.GPTResponse.content.appointment}</p>
                </div>
            )}

            {'genericTask' in props.GPTResponse.content && (
                <div>
                    <img
                        className="integrationIMG"
                        src={TrelloIMG}
                        alt="trello_img"
                    />
                    <p>{props.GPTResponse.content.genericTask}</p>
                </div>
            )}

            {'mail' in props.GPTResponse.content && (
                <div>
                    <img
                        className="integrationIMG"
                        src={GmailIMG}
                        alt="gmail_img"
                    />
                    <p>{props.GPTResponse.content.mail}</p>
                </div>
            )}

            {/* {'feedback' in props.GPTResponse.content && (
                <div>
                    <img
                        className="integrationIMG"
                        src={FeedbackIMG}
                        alt="feedback_img"
                    />
                    <p>{props.GPTResponse.content.feedback}</p>
                </div>
            )} */}
        </div>
    );
};

export default ResultOverview;

# Getting Started

## Development

Build and tag the Docker image

```bash
docker build -t sample:dev .
```

Spin up the container once the build is done and visit [http://localhost:3001/](http://localhost:3001/)::

```bash
docker run \
    -it \
    --rm \
    -v ${PWD}:/app \
    -v /app/node_modules \
    -p 3001:3000 \
    -e CHOKIDAR_USEPOLLING=true \
    sample:dev
```

## Production

Build and tag the Docker image:

```bash
docker build -f Dockerfile.prod -t sample:prod .
```

Spin up the container and visit [http://localhost:8080/](http://localhost:8080/):

```bash
docker run -it --rm -p 8080:80 sample:prod
```
